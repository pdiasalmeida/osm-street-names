import os.path
import overpy


class Helper:
    def __init__(self):
        self.overpassAPI = None

    def query_osm(self, osm_query):
        result = [self.query_overpass(osm_query).get_nodes(), self.query_overpass(osm_query).get_ways()]

        return result

    def query_overpass(self, overpass_query_arg):
        if self.overpassAPI is None:
            self.overpassAPI = overpy.Overpass()

        if os.path.isfile(overpass_query_arg):
            with open(overpass_query_arg, "r") as my_file:
                query = my_file.read()
        else:
            query = overpass_query_arg

        return self.overpassAPI.query(query)
