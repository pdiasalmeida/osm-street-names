#!/usr/bin/python

import argparse
import json
from os import listdir
from os.path import isfile, join
from sys import exit

import osmQuery
import knowledgeBaseQuery
from countries import countries

debug = True


def write_results_json(result, out_file):
    with open(out_file, "w") as text_file:
        text_file.write(json.dumps(result, indent=4))


def parse_json(file_path):
    with open(file_path) as data_file:
        return json.load(data_file)


def populate_thoroughfare(directory):
    result = {}
    only_files = [f for f in listdir(directory) if isfile(join(directory, f))]
    for f in only_files:
        with open(directory + '/' + f) as data_file:
            file_data = json.load(data_file)

            if 'language' in file_data:
                info = {}
                if 'prefix' in file_data:
                    info['prefix'] = file_data['prefix']
                if 'thoroughfare_list' in file_data:
                    info['thoroughfare_list'] = file_data['thoroughfare_list']
                if 'thoroughfare_art' in file_data:
                    info['thoroughfare_art'] = file_data['thoroughfare_art']

                result[file_data['language']] = info

    return result


class SemanticQuery:
    query_cache = dict()
    cache_hits = 0
    cc = countries.CountryChecker('./countries/TM_WORLD_BORDERS-0.3.shp')

    def __init__(self, config_path):
        self.thoroughfare = populate_thoroughfare('./thoroughfare')
        self.query_source = None
        self.query_result = None
        self.query_claims = None
        self.query_output = None

        self.config = parse_json(config_path)
        self.parse_config()

        self.kqh = knowledgeBaseQuery.Helper(debug, self.query_source,
                                             self.query_result, self.query_claims, self.query_output)

    def parse_config(self):
        if 'query' in self.config \
                and 'source' in self.config['query'] \
                and 'result' in self.config['query'] \
                and 'claims' in self.config['query'] \
                and 'output' in self.config['query']:
            self.query_source = self.config['query']['source']
            self.query_result = self.config['query']['result']
            self.query_claims = self.config['query']['claims']
            self.query_output = self.config['query']['output']

            return True
        else:
            return False

    def fill_wiki_tags_geosjon(self, result, lang):
        geojson = {
                "type": "FeatureCollection",
                "features": []
        }

        for way in result[1]:
            way.tags["@id"] = "way/" + str(way.id)

            coordinates = []
            for nd in way.get_nodes():
                lonlat = [float(nd.lon), float(nd.lat)]
                coordinates.append(lonlat)

            # TODO: get query associated country (use coordinates associated with the way or feature)
            country = None

            properties = {}
            for tag in way.tags:
                properties[tag] = way.tags[tag]

            final_properties = properties.copy()
            if 'name' in way.tags:
                # process feature
                query = way.tags['name']
                if query in self.query_cache:
                    qsw_res = self.query_cache[query]
                    self.cache_hits += 1
                else:
                    # self.get_wiki_tags(query, lang)
                    qsw_res = self.kqh.match_entity_id(query, country, lang)
                    self.query_cache[query] = qsw_res

                if qsw_res is not None:
                    final_properties.update({'wikidata': qsw_res})

                # process etimology
                query = self.process_street_names(way.tags['name'], lang)
                if query in self.query_cache:
                    qsw_res = self.query_cache[query]
                    self.cache_hits += 1
                else:
                    qsw_res = self.kqh.match_entity_id(query, country, lang)
                    self.query_cache[query] = qsw_res

                final_properties = properties.copy()
                if qsw_res is not None:
                    final_properties.update({'name:etimology:wikidata': qsw_res})

            geojson["features"].append({
                "type": "Feature",
                "id": "way/" + str(way.id),
                "properties": final_properties,
                "geometry": {
                    "type": "LineString",
                    "coordinates": coordinates,
                },
            })

        return geojson

    def fill_wiki_tags_query(self, query, language):
        # TODO: find country associated with the query (pass as argument?)
        country = None
        query = self.process_street_names(query, language)
        return self.kqh.match_entity_id(query, country, language)

    def process_street_names(self, street_name, lang):
        words = street_name.split()
        result = street_name

        n = 0
        if lang in self.thoroughfare and self.thoroughfare[lang]['prefix'] is True:
            if words[0] in self.thoroughfare[lang]['thoroughfare_list']:
                n += 1
                if words[n] in self.thoroughfare[lang]['thoroughfare_art']:
                    n += 1

            result = ' '.join(words[n:])

        if lang in self.thoroughfare and self.thoroughfare[lang]['prefix'] is False:
            s = len(words)-1
            if words[s] in self.thoroughfare[lang]['thoroughfare_list']:
                s -= 1
                if words[s].endswith("\'s"):
                    words[s] = words[s].rstrip("\'s")
            else:
                for w in self.thoroughfare[lang]['thoroughfare_list']:
                    if words[s].upper().endswith(w.upper()):
                        words[s] = words[s].upper().rstrip(w.upper())

            result = ' '.join(words[:s+1])

        return result


if __name__ == "__main__":
    ap = argparse.ArgumentParser()
    ap.add_argument("config", help="<path to config file>")
    ap.add_argument("-q", help="<file or string with osm query>")
    ap.add_argument("-s", help="<query string>")
    ap.add_argument("-o", help="<output file>")
    ap.add_argument("-lng", default="en", help="<desired language>")

    opts = ap.parse_args()

    error_msg = ''
    if opts.config is not None:
        sqc = SemanticQuery(opts.config)
        r = None

        if sqc.config is not None:
            lng = opts.lng
            if opts.q is not None:
                # query overpass can possibly also return country to use in language
                osm_qhlp = osmQuery.Helper()
                ovp = osm_qhlp.query_osm(opts.q)

                print('running semantic query for overpass result')
                r = sqc.fill_wiki_tags_geosjon(ovp, lng)
            elif opts.s is not None:
                print('running semantic query for query string')
                r = sqc.fill_wiki_tags_query(opts.s, lng)

        if r is not None:
            if opts.o is not None:
                write_results_json(r, opts.o)
            else:
                print r
            print('done')
        else:
            print('empty result')

        # print sqc.cache_hits
        # print map
    else:
        print('exit with error: ' + error_msg)
        exit(1)

    exit(0)
