from countries import countries

import urllib
import json
import time


def build_get_entities_query(query):
    query_aux = query.split()
    query = "select ?s1 as ?c1, (bif:search_excerpt (bif:vector (" + get_individual_words(query_aux) + ") , ?o1)) as ?c2, ?sc, ?rank, ?g where\
            { \
                {\
                    {\
                        select ?s1, (?sc * 3e-1) as ?sc, ?o1, (sql:rnk_scale (<LONG::IRI_RANK> (?s1))) as ?rank, ?g where\
                        {\
                            quad map virtrdf:DefaultQuadMap\
                            {\
                                graph ?g\
                                {\
                                    ?s1 ?s1textp ?o1 .\
                                    ?o1 bif:contains \' (" + get_words_condition(query_aux) + ") \' option (score ?sc) .\
                                }\
                            }\
                        }\
                    order by desc (?sc * 3e-1 + sql:rnk_scale (<LONG::IRI_RANK> (?s1))) limit 20 offset 0\
                    }\
                }\
            }"

    return query


def build_get_entity_label_query(entity_id, lang):
    query = "PREFIX wd: <http://www.wikidata.org/entity/>\
            prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#>\
            SELECT ?value WHERE { \
                wd:Q" + str(entity_id) + " rdfs:label ?value FILTER(LANG(?value) = \"" + lang + "\" || LANG(?value) = \"en\").\
            }"

    return query


def get_individual_words(word_vec):
    result = str()
    first = True
    for word in word_vec:
        if first is not True:
            result += ", "
        result = result + "\'" + word.upper() + "\'"
        first = False

    return result


def get_words_condition(word_vec):
    result = str()
    first = True
    for word in word_vec:
        if first is not True:
            result += " AND "
        result = result + word.upper()
        first = False

    return result


def get_dict_object(key, dic):
    result = None
    key_aux = key.split('|')
    if key_aux[0] in dic:
        result = dic[key_aux[0]]
        for n in range(1, len(key_aux)):
            if key_aux[n] in result:
                result = result[key_aux[n]]
            elif key_aux[n] == "value" and "somevalue" in result:
                result = result["somevalue"]

    return result


class DataEntity:
    _type_value = "data entity"

    def __init__(self, entity_attributes=None, entity_claims=None):
        self.attributes = entity_attributes
        self.claims = entity_claims
        self.country = None

    def get_entity_country(self, cc):
        if 'entity_country' in self.claims and not self.claims['entity_country'] == 'undefined':
            self.country = self.claims['entity_country']
        elif 'entity_country_citizen' in self.claims and not self.claims['entity_country_citizen'] == 'undefined':
            self.country = self.claims['entity_country_citizen']
        elif 'entity_country_origin' in self.claims and not self.claims['entity_country_origin'] == 'undefined':
            self.country = self.claims['entity_country_origin']
        elif 'entity_lat' in self.claims and 'entity_lon' in self.claims\
                and not self.claims['entity_lat'] == 'undefined' and not self.claims['entity_lon'] == 'undefined':
            try:
                self.country = cc.getCountry(countries.Point(self.claims['entity_lat'], self.claims['entity_lon'])).name
            except AttributeError:
                print "Error in get entity country for entity " + self.attributes['entity_id'] + ":"
                print "    lat: " + str(self.claims['entity_lat'])
                print "    lon: " + str(self.claims['entity_lon'])

        return self.country

    def get_wikipedia_entries(self):
        n = 1
        query = "PREFIX schema: <http://schema.org/>\
            PREFIX wikibase: <http://wikiba.se/ontology#>\
            PREFIX wd: <http://www.wikidata.org/entity/>\
            PREFIX wdt: <http://www.wikidata.org/prop/direct/>\
            SELECT ?value WHERE { \
                ?value schema:about wd:" + str(self.attributes['wikidata']) + ". \
                FILTER( regex(str(?value), \"wikipedia\" )) \
            }"

        status, result = Helper.query_wikidata_sparql(query, 1)
        if status is True:
            n = len(result)

        return n

    def to_dict(self):
        result = {}
        if self.attributes is not None:
            final = result.copy()
            final.update(self.attributes)
            final.update(self.claims)
            result = final

        return result

    def set_claims(self, claims):
        self.claims = claims


class QueryResult:
    _type_value = "query result"

    def __init__(self, query=None, country=None):
        self.query = query
        self.country = country
        self.query_results = []

    def set_query_results(self, results):
        for result in results:
            query_result = {'confidence': -1, 'value': result}
            self.query_results.append(query_result)

    def score_results(self, country_checker):
        for query_result in self.query_results:
            score = self.score_result(query_result['value'], country_checker)
            query_result['confidence'] = round(100*score, 2)

    def score_result(self, result, country_checker):
        entity_country = result.get_entity_country(country_checker)
        country_weight = 0.7
        country_score = 0
        entity_relevance_weight = 0.3
        wiki_entries = result.get_wikipedia_entries()
        wiki_entries_min = 0
        wiki_entries_max = 50  # 165

        if self.country is not None and entity_country == self.country:
            country_score = 1
            if 'confidence_reason' not in result.attributes:
                result.attributes['confidence_reason'] = []
            result.attributes['confidence_reason'].append('Entity in the same country as query')

        # number of wikipedia pages in various languages
        entity_relevance_score = min(1, (wiki_entries-wiki_entries_min) / ((wiki_entries_max-wiki_entries_min)*1.0))
        if entity_relevance_score > 0.5:
            if 'confidence_reason' not in result.attributes:
                result.attributes['confidence_reason'] = []
            result.attributes['confidence_reason'].append('Entity has high degree of relevance')

        return country_weight*country_score + entity_relevance_weight*entity_relevance_score

    def expose_scores(self):
        self.query_results = sorted(self.query_results, key=lambda result: result['confidence'], reverse=True)
        for query_result in self.query_results:
            query_result['value'].attributes['confidence'] = query_result['confidence']

    def get_top_entity(self):
        if len(self.query_results) > 0:
            self.query_results = sorted(self.query_results, key=lambda result: result['confidence'], reverse=True)
            return self.query_results[0]['value']
        else:
            return None


class Helper:
    cc = countries.CountryChecker('./countries/TM_WORLD_BORDERS-0.3.shp')

    def __init__(self, debug, query_source, query_result, query_claims, query_output):
        self.debug = debug
        self.query_source = query_source
        self.query_result = query_result
        self.query_claims = query_claims
        self.query_output = query_output

    def getCountryByLang(self, language):
        if language == 'pt':
            return 'Portugal'
        if language == 'it':
            return 'Italy'
        if language == 'en':
            return 'England'

    def match_entity_id(self, query_string, country, language):
        result = None
        status = False
        data_entities = None
        
        # TODO: improve country detection
        if country is None:
            country = self.getCountryByLang(language)

        query_result = QueryResult(query_string, country)

        if self.query_source['type'] == "wikidata":
            status, data_entities = self.get_entities_wikidata(query_string, language)
        elif self.query_source['type'] == "wikipedia":
            status, data_entities = self.get_entities_wikipedia(query_string, language)

        if self.debug is True:
            result = {'query': query_string}
        if status is True:
            if self.query_output['result'] == "first":
                query_result.set_query_results(data_entities)
                data_entity = query_result.get_top_entity()

            elif self.query_output['result'] == "best":
                data_entities = self.get_entity_claims_wikidata(data_entities, self.query_claims, language)
                query_result.set_query_results(data_entities)
                query_result.score_results(self.cc)
                data_entity = query_result.get_top_entity()

            elif self.query_output['result'] == "all":
                query_result.set_query_results(data_entities)
                data_entity = query_result.query_results

            else:  # self.query_output['result'] == "all-sorted":
                data_entities = self.get_entity_claims_wikidata(data_entities, self.query_claims, language)
                query_result.set_query_results(data_entities)
                query_result.score_results(self.cc)
                query_result.expose_scores()
                data_entity = query_result.query_results

            if type(data_entity) is list:
                result = []
                for de in data_entity:
                    result.append(de['value'].to_dict())
            else:
                if self.debug is True:
                    result.update(data_entity.to_dict())
                else:
                    result = data_entity.attributes[self.query_output['idAttribute']]

        return result

    ##
    # HANDLE ENTITIES #
    ##

    def get_entities_wikidata(self, query_string, lang):
        status = False

        if 'sparql' in self.query_source and self.query_source['sparql'] is True:
            query_string = build_get_entities_query(query_string)

        # TODO: solve variable encoding problems
        params = {self.query_source['queryParam']: query_string.encode('utf-8'),
                  self.query_source['languageParam']: lang}

        final_params = params.copy()
        if 'params' in self.query_source:
            final_params.update(self.query_source['params'])

        query_params = urllib.urlencode(final_params)
        freq = self.query_source['url'] + "?%s" % query_params
        q_result = json.load(urllib.urlopen(freq))

        result_entities = []
        qr_entities = []
        if self.query_result['param'] in q_result:
            qr_entities = q_result[self.query_result['param']]

        if len(qr_entities) > 0:
            status = True

        for n in range(0, len(qr_entities)):
            attributes = {}
            if 'properties' in self.query_result:
                for p in self.query_result['properties']:
                    if p["label"] in qr_entities[n]:
                        prop = qr_entities[n][p["label"]]
                        attributes[p['saveAs']] = prop

            entity = DataEntity(attributes)
            result_entities.append(entity)

        return status, result_entities

    def get_entities_wikipedia(self, query_string, lang):
        status = False
        # TODO: solve variable encoding problems
        params = {self.query_source['queryParam']: query_string.encode('utf-8'),
                  "redirects": "resolve"}

        final_params = params.copy()
        if 'params' in self.query_source:
            final_params.update(self.query_source['params'])

        query_params = urllib.urlencode(final_params)
        wiki_api_url = "https://" + lang + ".wikipedia.org/w/api.php"
        q_result = json.load(urllib.urlopen(wiki_api_url + "?%s" % query_params))

        result_entities = []
        if len(q_result[1]) > 0:
            status = True

        # TODO: get wikidata id from page title in wikipedia url
        # https://en.wikipedia.org/w/api.php?action=query&prop=pageprops&format=json&titles=Tom%20Hanks
        for n in range(0, len(q_result[1])):
            attributes = {}
            if 'properties' in self.query_result:
                for p in self.query_result['properties']:
                    attributes[p['saveAs']] = q_result[p['position']]

            entity = DataEntity(attributes)
            result_entities.append(entity)

        return status, result_entities

    # TODO: add dbpedia query;
    def get_entities_dbpedia(self):
        pass

    ##
    # HANDLE ENTITY IDS #
    ##

    # @deprecated
    def match_entity_id_wikidata(self, query_string, country, language):
        query_result = QueryResult(query_string, country)

        status1, data_entities = self.get_entities_wikidata(query_string, language)
        result = None
        if self.debug is True:
            result = {'query': query_string}

        if status1 is True:
            if self.query_output['result'] == "first":
                query_result.set_query_results(data_entities)
                data_entity = query_result.get_top_entity()

            elif self.query_output['result'] == "best":
                data_entities = self.get_entity_claims_wikidata(data_entities, self.query_claims, language)
                query_result.set_query_results(data_entities)
                query_result.score_results(self.cc)
                data_entity = query_result.get_top_entity()

            elif self.query_output['result'] == "all":
                query_result.set_query_results(data_entities)
                data_entity = query_result.query_results

            else:  # self.query_output['result'] == "all-sorted":
                data_entities = self.get_entity_claims_wikidata(data_entities, self.query_claims,  language)
                query_result.set_query_results(data_entities)
                query_result.score_results(self.cc)
                query_result.expose_scores()
                data_entity = query_result.query_results

            if type(data_entity) is list:
                result = []
                for de in data_entity:
                    result.append(de['value'].to_dict())
            else:
                if self.debug is True:
                    result.update(data_entity.attributes)
                else:
                    result = data_entity.attributes['wikidata']

        return result

    ##
    # HANDLE ENTITY CLAIMS #
    ##

    @staticmethod
    def get_entity_claims_wikidata(list_entities, query_base, lang):
        result = []
        status = True

        if query_base is not None:
            for entity in list_entities:
                if entity is not None and 'wikidata' in entity.attributes:
                    params = {'entity': entity.attributes['wikidata']}
                    final_params = params.copy()
                    if 'params' in query_base['source']:
                        final_params.update(query_base['source']['params'])

                    query_params = urllib.urlencode(final_params)
                    qr = json.load(urllib.urlopen(query_base['source']['url'] + "?%s" % query_params))
                    # print(qr)

                    claims = {}
                    if query_base['result']['param'] in qr:
                        eva = qr[query_base['result']['param']]
                        # remove wikipedia disambiguation pages
                        if 'P31' in eva:
                            for c in eva['P31']:
                                if c['mainsnak']['datatype'] == 'wikibase-item' \
                                        and c['mainsnak']['datavalue']['value']['numeric-id'] == 4167410:
                                    status = False

                        if status is True and 'properties' in query_base['result']:
                            for prop in query_base['result']['properties']:
                                if prop['id'] in eva:
                                    val = get_dict_object(prop['value'], eva[prop['id']][0])
                                    if prop['query-label'] is True:
                                        val = Helper.get_entity_label(val, lang)
                                    claims[prop['label']] = val
                                else:
                                    claims[prop['label']] = 'undefined'

                        entity.set_claims(claims)

                result.append(entity)

        return result

    ##
    # AUXILIARY #
    ##

    @staticmethod
    def query_wikidata_sparql(query, mode):
        status = False
        result = None
        params = urllib.urlencode({'query': query, 'format': 'json'})

        try:
            qr = json.load(urllib.urlopen("https://query.wikidata.org/bigdata/namespace/wdq/sparql?%s" % params))

            if len(qr['results']['bindings']) > 0:
                if mode == 0:
                    eva = qr['results']['bindings'][0]
                    status = True
                    if 'value' in eva['value']:
                        result = eva['value']['value']
                    else:
                        print eva
                else:
                    result = qr['results']['bindings']
                    status = True

        except IOError, e:
            if e.errno == 101:
                print "Network Error"
                time.sleep(1)
                Helper.query_wikidata_sparql(query, mode)
            else:
                raise
        except ValueError:
            print "Error in query wikidata sparql"
            print "    Query: " + query
            print "    params: " + "https://query.wikidata.org/bigdata/namespace/wdq/sparql?%s" % params

        return status, result

    @staticmethod
    def get_entity_label(entity_id, lang):
        status, result = Helper.query_wikidata_sparql(build_get_entity_label_query(entity_id, lang), 0)
        return result
