# Where the streets have known names

Providing semantic knowledge to street names.

## Approach

In order to associate a street name with semantic information relating to its namesake we start by combining OSM results with a knowledge base. In this initial stage we focused on [Wikidata](https://www.wikidata.org).

Our solution starts by executing an overpass query and processing its results, taking each street name and building a query for the chosen knowledge base. In preparation for this query the street names have to be filtered in order to extract only their namesake, ignoring the thoroughfare type (i.e. 'Via Panfilo Castaldi' results in the query for 'Panfilo Castaldi').

## Preliminary results

This script was tested, as example, for a central area of Rome, including the neighborhoods of Trastevere and Testaccio, with the following overpass query:

    [out:json];
    (
        way
            ["highway"="residential"]["name"]
            (41.8642593,12.4612841,41.9030756,12.4945021)
    );
    out body;
    >;
    out skel qt;
And for the 1709 retrieved roads with names, it managed to match Wikidata entities to 1121.

This association of street names with known entities in a knowledge base already allows for some analysis over the properties and characteristics of the namesakes chosen in each area.

As example we can look for how the streets named after human entities divide between males and females. As we can see, only approximately 6% (36 instances) are named after females.

![gender of human entities](./results/rome/qgis/gender_f.png)

We can also analyse the field of occupation of each of these human namesakes. For this region of Italy we have as the top 3 fields of activity as science (109 instances), religion (90 instances), and politics (84 instances). The analysis of the following map also brings to attention a small curiosity in the existence of some clusters of streets named after writers, scientists, and politicians in this region of Italy.

![field of occupation of human entities](./results/rome/qgis/field_f.png)

## Dependencies

This solution requires the following dependencies:

* Python 2.7
* [overpy](https://pypi.python.org/pypi/overpy)

## Usage

This solution has two possible operation modes, either working on an overpass query or on an individual street name. For the first mode the following command should be used:

    .: ./semOSM.py -q test_query.ovp -lng "pt" -o out.json config.json

Where 'sample.q' contains an overpass query, 'it' represents the language we prefer for the retrieved labels and descriptions, 'out.json' is the file to store the output, and 'config.cfg' is the configuration file.

To process an individual street name we use the argument '-s', as follows:

    .: ./semOSM.py -s "Via Panfilo Castaldi" -lng "it" -o out.json config.json


In the configuration file we can define the source for the knowledge base used, as well as the parameters for each query.
Also in the configuration file, we can choose the output mode of the queries. There are four output modes:

 * 'first' -> the top result returned from our knowledge base
 * 'best' -> the top result, after applying our scoring function
 * 'all' -> all the entities retrieved for the query
 * 'all-sorted' -> all the entities retrieved ordered by our scoring function
